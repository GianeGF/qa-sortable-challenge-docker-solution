// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

let SpecReporter = require('jasmine-spec-reporter').SpecReporter;

exports.config = {
  allScriptsTimeout: 110000,
  capabilities: {
    'browserName': 'chrome',
  },
  suites: {
    test: ['./table-page.spec.js'],
  },
  directConnect: true,
  seleniumAddress: 'http://localhost:4444/wd/hub',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function () { }
  },
  onPrepare() {
    jasmine.getEnv().addReporter(new SpecReporter({ 
      spec: { displayStacktrace: true, displayDuration: true },
      summary: { displayDuration: true } 
    }));
  }
};
