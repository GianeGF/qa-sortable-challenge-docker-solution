var TablePage = function() {

    this.item0 = element(by.cssContainingText('li', 'Item 0'));
    this.item1 = element(by.cssContainingText('li', 'Item 1'));
    this.item2 = element(by.cssContainingText('li', 'Item 2'));
    this.item3 = element(by.cssContainingText('li', 'Item 3'));
    this.item4 = element(by.cssContainingText('li', 'Item 4'));
    this.item5 = element(by.cssContainingText('li', 'Item 5'));
    this.itemList = element.all(by.cssContainingText('li', 'Item '));

    this.setFirstItemToFive = async function() {
        var firstItem = await this.itemList.get(0).getText();
        await console.log('First table item when the page is loaded: ' + firstItem);
        //Guarantee that the first item in table is the Item 5
        if (this.itemList.get(0).getText() != "Item 5") {
          await browser.actions().
          mouseDown(this.item5).
          mouseMove(this.itemList.get(0)).
          mouseUp().
          perform();
          firstItem = await await this.itemList.get(0).getText();
          await console.log('New first table item: ' + firstItem);
        }
    };

    this.sortTable = async function() {
        await browser.actions().mouseDown(this.item4).
          mouseMove(this.item5).
          mouseUp().
          perform();
        browser.driver.sleep(2000);
        await browser.actions().
          mouseDown(this.item3).
          mouseMove(this.item4).
          mouseUp().
          perform();
        browser.driver.sleep(2000);
        await browser.actions().
          mouseDown(this.item2).
          mouseMove(this.item3).
          mouseUp().
          perform();
        browser.driver.sleep(2000);
        await browser.actions().
          mouseDown(this.item1).
          mouseMove(this.item2).
          mouseUp().
          perform();
        browser.driver.sleep(2000);
        await browser.actions().
          mouseDown(this.item0).
          mouseMove(this.item1).
          mouseUp().
          perform();
        browser.driver.sleep(2000);
    };
};

module.exports = new TablePage();