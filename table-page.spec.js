var tablePage = require('./table-page');

describe('QA Sortable Challenge', function() {
    
    beforeEach(function() {
      browser.waitForAngularEnabled(false);
      browser.get('http://localhost:3000');
    });

    it('should open page', async function() {
      expect(await browser.getCurrentUrl()).toBe('http://localhost:3000/');
    });

    it('should there be six elements in the table to sort', async function() {
      expect(await tablePage.itemList.count()).toEqual(6);
    });
  
    it('items should have correct text', async function() {
      expect(await tablePage.item0.isDisplayed()).toBe(true);
      expect(await tablePage.item1.isDisplayed()).toBe(true);
      expect(await tablePage.item2.isDisplayed()).toBe(true);
      expect(await tablePage.item3.isDisplayed()).toBe(true);
      expect(await tablePage.item4.isDisplayed()).toBe(true);
      expect(await tablePage.item5.isDisplayed()).toBe(true);
    });

    it('table should be sorted from 0 to 5 by drag and drop actions', async function() {

      /* The only purpose of sleep usage in this test - even inside the sortTable function - 
      is to allow a person to follow the test execution on browser as it happens.
      That way every step can be seen. The test itself does not depend on sleeps to perform. */

      browser.driver.sleep(3000);
      await tablePage.setFirstItemToFive();
      browser.driver.sleep(2000);
      await tablePage.sortTable();

      expect(await tablePage.itemList.get(0).getText()).toBe("Item 0");
      expect(await tablePage.itemList.get(5).getText()).toBe("Item 5");
    });

  });